package com.ionos.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ionos.server.Server;

public class Client implements Runnable {
	final static Logger logger = LoggerFactory.getLogger(Client.class);

	/*
	 * The Constant port
	 * */
	private final String PORT="port";
	
	/*
	 * The constant server name
	 * */
	private final String SERVER_NAME = "server";
	
	private Properties properties;

	private String serverName;

	public Properties getProperties() {
		return properties;
	}

	private int port;

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}


	public Client(Properties properties) {
		this.properties = properties;
		initialize();
	}
	public void initialize(){
		this.setPort(Integer.parseInt(this.getProperties().getProperty(PORT)));
		this.setServerName(this.getProperties().getProperty(SERVER_NAME));
	}
	public void run() {
		Socket client = null;
		DataOutputStream out = null;
		DataInputStream in = null;
		try
		{	

			final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
			if (logger.isDebugEnabled()) {
				logger.debug(Thread.currentThread().getName());
				logger.debug("Connecting to " + serverName + " on port " + port);

			}

			client = new Socket(serverName, port);

			if (logger.isDebugEnabled()) {
				logger.debug("Just connected to " + client.getRemoteSocketAddress());

			}

	        rwl.writeLock().lock();
			//System.out.println("Just connected to " + client.getRemoteSocketAddress());
			OutputStream outToServer = client.getOutputStream();
			out = new DataOutputStream(outToServer);
			out.writeUTF("Hello from "+ client.getLocalSocketAddress() + "  Alive");
			out.writeUTF("Client sends=======================>Event1");
			rwl.readLock().lock();
			rwl.writeLock().unlock(); // Unlock write, still hold read
			InputStream inFromServer = client.getInputStream();
			in = new DataInputStream(inFromServer);
			System.out.println("Client says " + in.readUTF());
			///////////////////////////////////////////////////
			rwl.readLock().unlock();
	        rwl.writeLock().lock();
			out.writeUTF("Client sends====================>Event3");
			out.writeUTF("Client says=====================>Terminating");
			////////////////////////////////////////////////
			rwl.writeLock().unlock();
			
			if (logger.isDebugEnabled()) {
				logger.debug(Thread.currentThread().getName()+" End.");

			}
			
			
		//System.out.println(in.readUTF());
		
		//System.out.println(Thread.currentThread().getName()+" End.");

	}
	catch(IOException e)
	{
		e.printStackTrace();
	}
		finally{
			try {
				in.close();
				out.close();
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		/*catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
}
}