package com.ionos.client;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Properties;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientThreadPoolMain {

	/** The Constant PREFIX. */
	private static final String PREFIX = "-";
	/**The logger object*/
	final static Logger logger = LoggerFactory.getLogger(Client.class);

	/** The Constant OPTIONS. */
	private static final Options OPTIONS = createOptions();

	public static void main(String[] args) {
		process(args);
	}

	private static void process(String[] args) {
		CommandLineParser parser = new BasicParser();
		ThreadPool pool= new ThreadPool();
			
		try {
			parser.parse(OPTIONS, args, true);
			CommandLine commandLine = parser.parse(OPTIONS, args, true);
			String[] commands = commandLine.getArgs();
			if (commands.length > 0) {
				inValidCommands(commandLine, commands);
			}
		
			Option[] optionArray = commandLine.getOptions();

			if (optionArray != null && optionArray.length > 0) {
				pool.setProperties(new Properties());
				for (Option option : optionArray) {
					String optionName = option.getOpt();
						String optionValue = commandLine.getOptionValue(optionName);
						
						pool.getProperties().setProperty(optionName, optionValue);
				}
			}
			pool.initialize();
			pool.execute();
			
		} catch (ParseException e) {
				e.printStackTrace();
		}
	}

	private static Options createOptions() {
		Options options = new Options();
		options.addOption("threads", true, "number of threads");
		options.addOption("server", true, "server name");
		options.addOption("port", true, "port number");
		return options;
	}

	private static void inValidCommands(CommandLine argCommandLine,
			String[] argCommands) {
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(
				System.err));
		for (String command : argCommands) {
			if (command.startsWith(PREFIX)) {
				if (!OPTIONS.hasOption(command)) {
					printWriter.write("In valid command " + command + "\n");
					printWriter.flush();
				}
			}
		}
		printUsage();
	}

	/**
	 * Prints the usage.
	 */
	private static void printUsage() {
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(
				System.err));
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printUsage(printWriter, 72,
				ClientThreadPoolMain.class.getName(), OPTIONS);
		helpFormatter
				.printHelp(printWriter, 72,
						ClientThreadPoolMain.class.getName(), null, OPTIONS, 0,
						0, null);
		printWriter.flush();
		System.exit(-1);
	}
}