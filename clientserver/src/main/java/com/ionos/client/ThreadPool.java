package com.ionos.client;

import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadPool {
	/** The logger object */
	final static Logger logger = LoggerFactory.getLogger(ThreadPool.class);
	/** Number of threads in thread pool */
	private int numberOfThreads;

	/** The properties. */
	private Properties properties;

	/**
	 * Constant threads
	 * 
	 * */
	private final String THREADS = "threads";

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public int getNumberOfThreads() {
		return numberOfThreads;
	}

	public void setNumberOfThreads(int numberOfThreads) {
		this.numberOfThreads = numberOfThreads;
	}

	public void initialize(){
		logger.info("Initializing the thread pool");
		this.setNumberOfThreads(Integer.parseInt(this.getProperties().getProperty(THREADS)));
	}
	
	public void execute() {
		ExecutorService executor = Executors
				.newFixedThreadPool(numberOfThreads);
		for (int i = 0; i < numberOfThreads; i++) {
			Runnable worker = new Client(this.getProperties());
			executor.execute(worker);
		}
		executor.shutdown();
		logger.info("Finished all threads");

	}

}