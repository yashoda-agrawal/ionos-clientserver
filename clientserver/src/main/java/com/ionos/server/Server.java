package com.ionos.server;

import java.net.*;
import java.io.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server extends Thread {
	private ServerSocket serverSocket;
	final static Logger logger = LoggerFactory.getLogger(Server.class);

	public static boolean InterCommunicateflag = true;

	public Server(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		serverSocket.setSoTimeout(1000000);
	}

	public void run() {
		while (true) {
			try {
				logger.info("Starting the server...");
				if(logger.isDebugEnabled()){
				logger.debug("Waiting for client on port "
						+ serverSocket.getLocalPort());
				}
				Socket server = serverSocket.accept();
				if (logger.isDebugEnabled()) {
					logger.debug("connected to "
							+ server.getRemoteSocketAddress());
				}
				DataInputStream in = new DataInputStream(
						server.getInputStream());
				logger.info("Message from client " + in.readUTF());
				DataOutputStream out = new DataOutputStream(
						server.getOutputStream());
				out.writeUTF("server sends===============================>Event2");
				System.out.println(in.readUTF());
				System.out.println(in.readUTF());

				out.writeUTF("Thank you for connecting to "
						+ server.getLocalSocketAddress() + "\nGoodbye!");
				out.close();
				in.close();
				server.close();
			} catch (SocketTimeoutException se) {
				logger.error("Socket timed out", se);
				break;
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				break;
			}
		}
	}

	public static void main(String[] args) {
		int port = 6060;
		try {
			Thread t = new Server(port);
			t.start();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
